class Fixnum

def in_words
  case
  when self < 10
    under_ten
  when self < 20
    teens
  when self < 100
    tens
  when self < 1000
    hundreds
  when self < 1_000_000
    thousands
  when self < 1_000_000_000
    millions
  when self < 1_000_000_000_000
    billions
  when self < 1_000_000_000_000_000
    trillions
  end
end

def create_words(x, y)
  if self % x == 0
    after = ""
  else
    after = " " + (self % x).in_words
  end

  (self / x).in_words + y + after
end

def under_ten
  under_ten = ["zero", "one", "two", "three", "four", "five", "six", "seven",
  "eight", "nine"]
  under_ten[self]
end

def teens
  teens = ["ten", "eleven", "twelve", "thirteen", "fourteen",
  "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"]
  teens[self - 10]
end

def tens
  tens = ["", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy",
  "eighty", "ninety"]
  if self % 10 == 0
    tens[(self / 10)]
  else
    "#{tens[(self / 10)]} #{(self % 10).under_ten}"
  end
end

def hundreds
  create_words(100, " hundred")
end

def thousands
  create_words(1000, " thousand")
end

def millions
  create_words(1000000, " million")
end

def billions
  create_words(1000000000, " billion")
end

def trillions
  create_words(1000000000000, " trillion")
end

end
